import java.util.Random;
import java.util.Scanner;

//o * quer dizer que importamos todas as bibliotecas relativas à java.util.
//a biblioteca que estamos importando aqui é a Random, responsavel pelo tipo de variavel "Random"
public class TpORG {
    Scanner in = new Scanner(System.in);
    Random r = new Random();
    int[][] memoriaInstrucoes;
    int[] memoryData = new int[1000]; // criamos um vetor memoryData, com 1000 posições (RAM)
    int operador; // variável global ultilizada para operações
    int Null = 0;

    public static void main(String[] args) {
        new TpORG();
    }

    public TpORG() {

        MontaRam();
        waitForInstructions();
    }

    void MontaRam() {
        Random x = new Random(); // cria um construtor de numeros aleatorios, chamando o metodo Random da
                                 // bilioteca que importamos.
        for (int i = 0; i < 1000; i++) {
            memoryData[i] = x.nextInt(100); // vamos ter valores de 0 a 99 em cada uma das posições do nosso vetor
        }
    }

    void waitForInstructions() {

        memoriaInstrucoes = new int[100][5]; // cria uma matrix 100x5
        int i = 0, op = 0;
        do {
            System.out.printf(" ===============" + "\n MAQUINA VIRTUAL " + "\n ===============" + "\n "
                    + "Digite uma instrucao : " + "\n 1 - SOMA " + "\n 2 - SUBTRACAO " + "\n 3 - MULTIPLICACAO "
                    + "\n 4 - DIVISAO " + "\n 5 - EXPONENCIAL " + "\n 6 - RAIZ QUADRADA " + "\n 7 - HIPOTENUSA "
                    + "\n 8 - AREA DE TRIANGULO " + "\n 9 - RAIZ QUALQUER " + "\n Ou -1 para sair... >>");
            int[] mainInstruction = new int[5]; // registrador
            int operation = in.nextInt();
            mainInstruction[1] = r.nextInt(1000);
            mainInstruction[2] = r.nextInt(1000);
            mainInstruction[3] = r.nextInt(1000);
            mainInstruction[4] = r.nextInt(1000);

            if (operation > 9 || operation < -1) {
                System.out.println("\n Número inválido, o programa será encerrado.");
                break;
            } else {
                switch (operation) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5: {
                    System.out.println("\n Digite o primeiro numero: ");
                    int num1 = in.nextInt();
                    mainInstruction[0] = 2;
                    mainCPU(mainInstruction, num1, 1);// salvando
                    System.out.println("\n Digite o segundo numero: ");
                    int num2 = in.nextInt();
                    mainInstruction[0] = 2;
                    mainCPU(mainInstruction, num2, 2);

                    CPU(mainInstruction, operation);
                    memoriaInstrucoes[i] = mainInstruction;
                    i++;
                    break;
                }
                case 6: {
                    System.out.println("\n Digite o numero que deseja saber a raiz quadrada: ");
                    int num1 = in.nextInt();
                    mainInstruction[0] = 2;
                    mainCPU(mainInstruction, num1, 1);// salvando
                    CPU(mainInstruction, operation);
                    memoriaInstrucoes[i] = mainInstruction;
                    i++;
                    break;
                }
                case 7: {
                    System.out.println("\n Digite o valor do cateto adjacente: ");
                    int num1 = in.nextInt();
                    mainInstruction[0] = 2;
                    mainCPU(mainInstruction, num1, 1);// salvando
                    System.out.println("\n Digite o valor do cateto oposto: ");
                    int num2 = in.nextInt();
                    mainInstruction[0] = 2;
                    mainCPU(mainInstruction, num2, 2);
                    CPU(mainInstruction, operation);
                    memoriaInstrucoes[i] = mainInstruction;
                    i++;
                    break;
                }
                case 8: {
                    System.out.println("\n Digite o valor da altura do triangulo: ");
                    int num1 = in.nextInt();
                    mainInstruction[0] = 2;
                    mainCPU(mainInstruction, num1, 1);// salvando
                    System.out.println("\n Digite o valor da base do triangulo: ");
                    int num2 = in.nextInt();
                    mainInstruction[0] = 2;
                    mainCPU(mainInstruction, num2, 2);
                    CPU(mainInstruction, operation);
                    memoriaInstrucoes[i] = mainInstruction;
                    i++;
                    break;
                }
                case 9: {
                    System.out.println("\n Digite o valor do radicando da raiz a ser calculada: ");
                    int num1 = in.nextInt();
                    mainInstruction[0] = 2;
                    mainCPU(mainInstruction, num1, 1);// salvando
                    System.out.println("\n Digite o valor indice da raiz a ser calculada: ");
                    int num2 = in.nextInt();
                    mainInstruction[0] = 2;
                    mainCPU(mainInstruction, num2, 2);
                    CPU(mainInstruction, operation);
                    memoriaInstrucoes[i] = mainInstruction;
                    i++;
                    break;
                }
                case -1: {
                    CPU(mainInstruction, operation);
                    break;
                }

                }

            }
        } while (op != -1);

    }

    // void montarInstrucoesAleatorias() {
    /*
     * memoriaInstrucoes = new int[100][4]; // cria uma matrix 100x4 Random r = new
     * Random(); for (int i = 0; i < 99; i++) { int[] mainInstruction = new int[4];
     * mainInstruction[0] = r.nextInt(5); mainInstruction[1] = r.nextInt(1000);
     * mainInstruction[2] = r.nextInt(1000); mainInstruction[3] = r.nextInt(1000);
     * memoriaInstrucoes[i] = mainInstruction;
     * 
     * System.out.println("nova instrucao: opcode: " + mainInstruction[0] +
     * " end1: " + mainInstruction[1] + " end2: " + mainInstruction[2] + " end3: " +
     * mainInstruction[3]);
     * 
     * }
     * 
     * // inserindo a ultima instrucao do programa que é o HALT -> parar int[]
     * mainInstruction = new int[4]; mainInstruction[0] = -1; mainInstruction[1] =
     * -1; mainInstruction[2] = -1; mainInstruction[3] = -1;
     * 
     * memoriaInstrucoes[99] = mainInstruction;
     */
    // }

    int mainCPU(int[] mainInstruction, int num, int where) {
        int opcode = mainInstruction[0];
        switch (opcode) {
        case 0: {
            return Sum(mainInstruction);
        }

        case 1: {
            return sub(mainInstruction);
        }

        case 2: {
            storeInRAM(mainInstruction, num, where);
            break;
        }
        case 3: {
            return takeFromRAM(mainInstruction, where);
        }
        case -1: {
            System.exit(0);
            break;
        }
        }
        return 0;
    }

    void printResult(int[] instruction, int operation) {
        instruction[0] = 3;
        int num1 = mainCPU(instruction, Null, 1);
        int num2 = mainCPU(instruction, Null, 2);
        int num3 = mainCPU(instruction, Null, 3);

        switch (operation) {
        case 1: {
            System.out.println("Somando " + num1 + " com " + num2 + " e gerando " + num3);
            break;
        }
        case 2: {
            System.out.println("Subtraindo " + num1 + " por " + num2 + " e gerando " + num3);
            break;
        }
        case 3: {
            System.out.println("Multiplicando " + num1 + " por " + num2 + " e gerando " + num3);
            break;
        }
        case 4: {
            int num4 = mainCPU(instruction, Null, 4);
            if (num3 == -1 && num4 == -1) {
                System.out.println("NAO EH POSSIVEL FAZER DIVISAO COM DIVISOR 0");
            } else {
                System.out.println("Dividindo " + num1 + " por " + num2 + " e gerando " + num3 + " com resto " + num4);
            }

            break;
        }
        case 5: {
            System.out.println("Elevando " + num1 + " por " + num2 + " e gerando " + num3);
            break;
        }
        case 6: {
            System.out.println("Raiz quadrada de " + num1 + " = " + num3);
            break;
        }
        case 7: {
            System.out.println("Hipotenusa de triangulo de catetos " + num1 + " e " + num2 + " = " + num3);
            break;
        }
        case 8: {
            System.out.println("Area do triangulo de altura " + num1 + " e base " + num2 + " = " + num3);
            break;
        }
        case 9: {
            if (num3 == -9876) {
                System.out.println("Nao existe raiz de indice " + num2);
            } else if (num3 == -6789) {
                System.out.println(
                        "Nao existe um resultado inteiro para a raiz de indice " + num2 + " de radicando " + num1);
            } else if (num3 == -9999) {
                System.out.println("Nao existe resultado para uma raiz de indice par e radicando impar");
            } else {
                System.out.println("A raiz de indice " + num2 + " e de radicando " + num1 + " eh " + num3);
            }

            break;
        }

        }

    }

    void CPU(int[] mainInstruction, int operation) { // recebe uma linha da matriz memoryData
        // interpreta os dados do registrador (registrador apontando os dados da RAM)
        int result;
        switch (operation) {
        // Soma
        case 1: {
            System.out.println("\n SOMA");
            mainInstruction[0] = 0;
            result = mainCPU(mainInstruction, Null, Null);// fazendo a soma
            mainInstruction[0] = 2;
            mainCPU(mainInstruction, result, 3);// salvando na memória

            printResult(mainInstruction, 1);
            break;

        }

        case 2: {
            System.out.println("\n SUBTRAÇÃO");
            mainInstruction[0] = 1;
            result = mainCPU(mainInstruction, Null, Null);// fazendo a subtração
            mainInstruction[0] = 2;
            mainCPU(mainInstruction, result, 3);// salvando na memória
            printResult(mainInstruction, 2);
            break;
        }

        case 3: {
            System.out.println("MULTIPLICACAO");
            multiply(mainInstruction);

            printResult(mainInstruction, 3);

            break;

        }
        case 4: {
            System.out.println("\n DIVISÃO :");

            divide(mainInstruction);
            printResult(mainInstruction, 4);

            break;

        }

        case 5: {
            System.out.println("\nEXPONENCIAL");
            exponential(mainInstruction);
            printResult(mainInstruction, 5);

            break;
        }

        case 6: {
            System.out.println("RAIZ QUADRADA");
            squareRoot(mainInstruction);
            printResult(mainInstruction, 6);

            break;

        }
        case 7: {
            System.out.println("CALCULAR HIPOTENUSA");
            hipotenusa(mainInstruction);
            printResult(mainInstruction, 7);

            break;
        }

        case 8: {
            System.out.println("\nAREA TRIANGULO");
            triangleArea(mainInstruction);
            printResult(mainInstruction, 8);

            break;
        }

        case 9: {
            System.out.println("\nRAIZ QUALQUER");
            root(mainInstruction);
            printResult(mainInstruction, 9);

            break;
        }
        case -1: {
            mainInstruction[0] = -1;
            System.out.println("O programa sera encerrado");
            mainCPU(mainInstruction, Null, Null);
        }

        }
    }

    int Sum(int[] mainInstruction) {

        // buscar na RAM
        mainInstruction[0] = 3;
        int conteudoRam1 = mainCPU(mainInstruction, Null, 1); // 1
        int conteudoRam2 = mainCPU(mainInstruction, Null, 2); // 2
        // faz a operação e retorna o valor
        int Sum = conteudoRam1 + conteudoRam2;
        return Sum;

    }

    void multiply(int[] mainInstruction) {

        int memAddress2 = mainInstruction[2];
        int memAddress3 = mainInstruction[3];

        mainInstruction[0] = 3;// retornar
        int ram1Content = mainCPU(mainInstruction, Null, 1);
        int ram2Content = mainCPU(mainInstruction, Null, 2);

        int result = 0;
        if (ram1Content == 1 || ram2Content == 1) {

            if (ram1Content == 1) {

                mainInstruction[0] = 2;// salva
                mainCPU(mainInstruction, ram2Content, 3);

            } else {
                mainInstruction[0] = 2;// salva
                mainCPU(mainInstruction, ram1Content, 3);

            }

        } else if (ram1Content == 0 || ram2Content == 0) {
            mainInstruction[0] = 2;// salva
            mainCPU(mainInstruction, 0, 3);
        } else {

            mainInstruction[0] = 3;
            int taken = mainCPU(mainInstruction, Null, 1);
            mainInstruction[0] = 2;
            mainCPU(mainInstruction, taken, 3);

            mainInstruction[2] = memAddress3;

            for (int i = 0; i < ram2Content - 1; i++) {

                mainInstruction[0] = 0;// soma
                result = mainCPU(mainInstruction, Null, Null);
                mainInstruction[0] = 2;// salva
                mainCPU(mainInstruction, result, 3);

            }

            mainInstruction[2] = memAddress2;

        }

    }

    void exponential(int[] mainInstruction) {

        int memAddress2 = mainInstruction[2];
        int memAddress3 = mainInstruction[3];
        mainInstruction[0] = 3;
        int ram1Content = mainCPU(mainInstruction, Null, 1);
        int ram2Content = mainCPU(mainInstruction, Null, 2);

        if (ram1Content == 1 || ram2Content == 1) {
            mainInstruction[0] = 2;
            mainCPU(mainInstruction, ram1Content, 3);

        } else if (ram2Content == 0) {
            mainInstruction[0] = 2;
            mainCPU(mainInstruction, 1, 3);
        } else {

            mainInstruction[0] = 3;
            int taken = mainCPU(mainInstruction, Null, 1);
            mainInstruction[0] = 2;
            mainCPU(mainInstruction, taken, 3);

            mainInstruction[2] = memAddress3;

            for (int i = 0; i < ram2Content - 1; i++) {

                multiply(mainInstruction);

            }

            mainInstruction[2] = memAddress2;

        }
    }

    void squareRoot(int[] mainInstruction) {

        mainInstruction[0] = 3;
        int result;
        int ram1Content = mainCPU(mainInstruction, Null, 1);
        mainInstruction[0] = 2;
        mainCPU(mainInstruction, ram1Content, 3);
        int cont = 0, aux = ram1Content;
        for (int i = 1; aux > 0; i += 2) {
            mainInstruction[0] = 2;
            mainCPU(mainInstruction, i, 2);
            mainCPU(mainInstruction, aux, 1);

            mainInstruction[0] = 1;
            result = mainCPU(mainInstruction, Null, Null);
            mainInstruction[0] = 2;
            mainCPU(mainInstruction, result, 3);
            mainInstruction[0] = 3;
            aux = mainCPU(mainInstruction, Null, 3);
            cont++;
        }
        mainInstruction[0] = 2;
        mainCPU(mainInstruction, ram1Content, 1);
        mainCPU(mainInstruction, cont, 3);
    }

    void triangleArea(int[] mainInstruction) {
        mainInstruction[0] = 3;
        int aux1 = mainCPU(mainInstruction, Null, 1);
        int aux2 = mainCPU(mainInstruction, Null, 2);
        multiply(mainInstruction);
        mainInstruction[0] = 3;
        int result1 = mainCPU(mainInstruction, Null, 3);
        mainInstruction[0] = 2;
        mainCPU(mainInstruction, result1, 1);
        mainCPU(mainInstruction, 2, 2);
        mainCPU(mainInstruction, 0, 3);
        divide(mainInstruction);
        mainInstruction[0] = 3;
        int result = mainCPU(mainInstruction, Null, 3);
        mainInstruction[0] = 2;
        mainCPU(mainInstruction, result, 3);
        mainCPU(mainInstruction, aux1, 1);
        mainCPU(mainInstruction, aux2, 2);

    }

    void divide(int[] mainInstruction) {
        int result = 1, cont = 0;

        int memAddress1 = mainInstruction[1];
        int memAddress2 = mainInstruction[2];

        mainInstruction[0] = 3;// trazer
        int ram1Content = mainCPU(mainInstruction, Null, 1);
        int RAM1CLONE = ram1Content;
        int ram2Content = mainCPU(mainInstruction, Null, 2);

        if (ram1Content < ram2Content) {
            mainInstruction[0] = 2;// salva
            mainCPU(mainInstruction, 0, 3);
            mainCPU(mainInstruction, ram1Content, 4);
        } else if (ram2Content == 0) {
            mainInstruction[0] = 2;// salva
            mainCPU(mainInstruction, -1, 3);
            mainCPU(mainInstruction, -1, 4);
        } else {
            while (ram1Content >= ram2Content) {

                mainInstruction[0] = 1;// subtrai
                result = mainCPU(mainInstruction, Null, Null);

                mainInstruction[0] = 2;
                mainCPU(mainInstruction, result, 3);// salvando

                mainInstruction[1] = mainInstruction[3];

                mainInstruction[0] = 3;// trazer
                ram1Content = mainCPU(mainInstruction, Null, 1);

                cont++;
            }
            mainInstruction[1] = memAddress1;
            mainInstruction[2] = memAddress2;//reestaura os endereços de memoria
            
            mainInstruction[0] = 2;//salva os conteudos nos lugares que a gente quer usar para fazer a mult
            mainCPU(mainInstruction, cont, 1);
            mainCPU(mainInstruction, ram2Content, 2);
            multiply(mainInstruction);
        
            mainCPU(mainInstruction, RAM1CLONE, 1);//salva a ram1 no lugar que quer utilizar p fazer subtração
            mainInstruction[0] = 3;
            int aux = mainCPU(mainInstruction, Null, 3);//coloca o resultado da mult pra participar da sub
    
            mainInstruction[0] = 2;//sub
            mainCPU(mainInstruction, aux, 2);
    
            mainInstruction[0] = 1;
            int rest = mainCPU(mainInstruction, Null, Null);
         
            mainInstruction[0] = 2;// salva
    
            mainCPU(mainInstruction, RAM1CLONE, 1);//reestaura conteudos
            mainCPU(mainInstruction, ram2Content, 2);
    
            mainCPU(mainInstruction, cont, 3);
    
            mainCPU(mainInstruction, rest, 4);

        }
        


    }

    int sub(int[] mainInstruction) {
        // sub
        int sub = 0;
        mainInstruction[0] = 3;
        int conteudoRam1 = mainCPU(mainInstruction, Null, 1); // 1
        int conteudoRam2 = mainCPU(mainInstruction, Null, 2); // 2

        sub = conteudoRam1 - conteudoRam2;

        return sub;

    }

    void hipotenusa(int[] mainInstruction) {

        int resultado = 0, resultado2 = 0, aux;
        int instruction[] = mainInstruction;
        instruction[0] = 3;
        int n1 = mainCPU(mainInstruction, Null, 1);
        aux = mainCPU(mainInstruction, Null, 2);
        instruction[0] = 2;
        mainCPU(mainInstruction, 2, 2);

        ////////////// inicio da exponenciação de a e b/////////////////
        exponential(instruction);
        instruction[0] = 3;
        resultado = mainCPU(mainInstruction, Null, 3);
        instruction[0] = 2;
        mainCPU(mainInstruction, resultado, 3);
        mainCPU(mainInstruction, aux, 1);
        exponential(instruction);
        instruction[0] = 3;
        resultado2 = mainCPU(mainInstruction, Null, 3);

        /////////////////////////// altera os valores na memoria principal///////////
        instruction[0] = 2;
        mainCPU(mainInstruction, resultado, 1);
        mainCPU(mainInstruction, resultado2, 2);
        resultado = 0;
        ////////////// inicio da nova instrução : soma ///////////////////////////
        resultado = Sum(instruction);
        instruction[0] = 2;
        mainCPU(mainInstruction, resultado, 1);

        ///////// guarda na memoria principal o resultado e inicia a ultima
        ///////// instrução//////////
        squareRoot(instruction);
        mainCPU(mainInstruction, n1, 1);
        mainCPU(mainInstruction, aux, 2);

    }

    void root(int[] mainInstruction) {
        mainInstruction[0] = 3;
        int ram1Content = mainCPU(mainInstruction, Null, 1);
        int ram2Content = mainCPU(mainInstruction, Null, 2);
        int rootToBeTaken = ram1Content;
        int kindOfRoot = ram2Content;

        int result;

        if (kindOfRoot == 1 || kindOfRoot == 0) {
            mainInstruction[0] = 2;
            mainCPU(mainInstruction, -9876, 3);
        } else if (kindOfRoot % 2 == 0 && rootToBeTaken < 0) {
            mainInstruction[0] = 2;
            mainCPU(mainInstruction, -9999, 3);
        } else if (kindOfRoot % 2 != 0 && rootToBeTaken < 0) {
            rootToBeTaken *= -1;
            for (int i = 0; i <= rootToBeTaken; i++) {
                mainInstruction[0] = 2;
                mainCPU(mainInstruction, i, 1);
                mainCPU(mainInstruction, kindOfRoot, 2);
                exponential(mainInstruction);
                mainInstruction[0] = 3;
                result = mainCPU(mainInstruction, Null, 3);

                if (result > rootToBeTaken) // se ele for maior que a raíz, ele não é a raíz e nenhum outro i vai ser //
                                            // pois a partir daqui os resultados só vão ultrapassar o valor do numero
                {
                    mainInstruction[0] = 2;
                    mainCPU(mainInstruction, -6789, 3);
                    mainCPU(mainInstruction, ram1Content, 1);
                    mainCPU(mainInstruction, ram2Content, 2);
                    break;
                } else if (result == rootToBeTaken) {// se o resultado for igual ao numero que vc quer tirar raíz, o i é
                                                     // a raiz desse numero
                    mainInstruction[0] = 2;
                    mainCPU(mainInstruction, -i, 3);
                    mainCPU(mainInstruction, ram1Content, 1);
                    mainCPU(mainInstruction, ram2Content, 2);
                    break;
                }

            }

        } else {
            for (int i = 0; i <= rootToBeTaken; i++) {
                mainInstruction[0] = 2;
                mainCPU(mainInstruction, i, 1);
                mainCPU(mainInstruction, kindOfRoot, 2);
                exponential(mainInstruction);
                mainInstruction[0] = 3;
                result = mainCPU(mainInstruction, Null, 3);

                if (result > rootToBeTaken) // se ele for maior que a raíz, ele não é a raíz e nenhum outro i vai ser //
                                            // pois a partir daqui os resultados só vão ultrapassar o valor do numero
                {
                    mainInstruction[0] = 2;
                    mainCPU(mainInstruction, -6789, 3);
                    mainCPU(mainInstruction, ram1Content, 1);
                    mainCPU(mainInstruction, ram2Content, 2);
                    break;
                } else if (result == rootToBeTaken) {// se o resultado for igual ao numero que vc quer tirar raíz, o i é
                                                     // a raiz desse numero
                    mainInstruction[0] = 2;
                    mainCPU(mainInstruction, i, 3);
                    mainCPU(mainInstruction, ram1Content, 1);
                    mainCPU(mainInstruction, ram2Content, 2);
                    break;
                }

            }
        }

    }

    void storeInRAM(int[] mainInstruction, int num, int where) {
        int end = mainInstruction[where];
        memoryData[end] = num; // tp2
    }

    int takeFromRAM(int[] mainInstruction, int where) {
        return memoryData[mainInstruction[where]];
    }
}